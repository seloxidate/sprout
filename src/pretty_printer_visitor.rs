use dracaena::ast::assignment::AssignmentStmt;
use dracaena::ast::assignment::*;
use dracaena::ast::declaration::*;
use dracaena::ast::eval::*;
use dracaena::ast::eval_name::*;
use dracaena::ast::factor::Factor;
use dracaena::ast::field::*;
use dracaena::ast::function::Function;
use dracaena::ast::local_body::LocalBody;
use dracaena::ast::qualifier::*;
use dracaena::ast::statement::ExpressionStatement;
use dracaena::ast::statement::Statement;
use dracaena::ast::*;
use dracaena::visitor::visitable::*;

#[derive(Default, PartialEq)]
pub(crate) struct PrettyPrinterVisitor {
    options: PrettyPrintOptions,
    source: String,
    num_decl_stmts: usize,
    current_decl_stmt_idx: usize,
    decl_stmts_formatted: Vec<DeclarationStmtFormatted>,
    num_stmts: usize,
    current_stmt_formatted: Option<StatementFormatted>,
    stmts_formatted: Vec<StatementFormatted>,
    complete_tern_expr_stmt_formatted: String,
}

impl<'a> Visitor<'a> for PrettyPrinterVisitor {
    fn visit_find_join(&mut self, _: &'a dracaena::ast::find::FindJoin) {}
    fn visit_find_where(&mut self, _: &'a dracaena::ast::find::FindWhere) {}
    fn visit_find_order(&mut self, _: &'a dracaena::ast::find::FindOrder) {}
    fn visit_find_using(&mut self, _: &'a dracaena::ast::find::FindUsing) {}
    fn visit_find_table(&mut self, _: &'a dracaena::ast::find::FindTable) {}
    fn visit_select_table(&mut self, _: &'a dracaena::ast::find::SelectTable) {}
    fn visit_select_stmt(&mut self, _: &'a dracaena::ast::find::SelectStmt) {}
    fn visit_select_opts(&mut self, _: &'a dracaena::ast::select_option::SelectOpts) {}
    fn visit_table(&mut self, _: &'a dracaena::ast::table::Table) {}
    fn visit_from(&mut self, _: &'a dracaena::ast::from::From) {}
    fn visit_index_hint(&mut self, _: &'a dracaena::ast::index_hint::IndexHint) {}
    fn visit_table_with_fields(&mut self, _: &'a dracaena::ast::table::TableWithFields) {}
    fn visit_join_spec(&mut self, _: &'a dracaena::ast::join::JoinSpec) {}
    fn visit_join_variant(&mut self, _: &'a dracaena::ast::join::JoinVariant) {}
    fn visit_join_clause(&mut self, _: &'a dracaena::ast::join::JoinClause) {}
    fn visit_join_using(&mut self, _: &'a dracaena::ast::join::JoinUsing) {}
    fn visit_join_order(&mut self, _: &'a dracaena::ast::join::JoinOrder) {}
    fn visit_order_group_data(&mut self, _: &'a dracaena::ast::order_group::OrderGroupData) {}
    fn visit_order_group(&mut self, _: &'a dracaena::ast::order_group::OrderGroup) {}
    fn visit_group_order_by(&mut self, _: &'a dracaena::ast::order_group::GroupOrderBy) {}
    fn visit_order_elem(&mut self, _: &'a dracaena::ast::order_elem::OrderElem) {}
    fn visit_direction(&mut self, _: &'a dracaena::ast::order_elem::Direction) {}
    fn visit_first_only(&mut self, _: &'a dracaena::ast::select_option::FirstOnly) {}
    fn visit_force_placeholder_literal(
        &mut self,
        _: &'a dracaena::ast::select_option::ForcePlaceholderLiteral,
    ) {
    }
    fn visit_function_expr(&mut self, _: &'a dracaena::ast::column::FunctionExpression) {}
    fn visit_column(&mut self, _: &'a dracaena::ast::column::Column) {}
    fn visit_conditional_expr(&mut self, _: &'a dracaena::ast::expression::ConditionalExpression) {}
    fn visit_conditional_expr_data(
        &mut self,
        _: &'a dracaena::ast::expression::ConditionalExpressionData,
    ) {
    }
    fn visit_expr(&mut self, _: &'a dracaena::ast::expression::Expression) {}
    fn visit_comparison_expr(&mut self, _: &'a dracaena::ast::expression::ComparisonExpression) {}
    fn visit_as_expr(&mut self, _: &'a dracaena::ast::expression::AsExpression) {}
    fn visit_is_expr(&mut self, _: &'a dracaena::ast::expression::IsExpression) {}
    fn visit_simple_expr(&mut self, _: &'a dracaena::ast::expression::SimpleExpression) {}
    fn visit_simple_expr_data(&mut self, _: &'a dracaena::ast::expression::SimpleExpressionData) {}
    fn visit_term(&mut self, _: &'a dracaena::ast::term::Term) {}
    fn visit_term_data(&mut self, _: &'a dracaena::ast::term::TermData) {}
    fn visit_complement_factor(&mut self, _: &'a dracaena::ast::factor::ComplementFactor) {}
    fn visit_second_factor(&mut self, _: &'a dracaena::ast::factor::SecondFactor) {}
    fn visit_factor(&mut self, node: &'a Factor) {}
    fn visit_constant(&mut self, _: &'a dracaena::ast::constant::Constant) {}
    fn visit_field(&mut self, _: &'a dracaena::ast::field::Field) {}
    fn visit_function(&mut self, _: &'a dracaena::ast::function::Function) {}
    fn visit_parm_elem(&mut self, _: &'a dracaena::ast::parm::ParmElem) {}
    fn visit_intrinsics(&mut self, _: &'a dracaena::ast::intrinsics::Intrinsics) {}
    fn visit_eval(&mut self, _: &'a dracaena::ast::eval::Eval) {}
    fn visit_eval_name(&mut self, _: &'a dracaena::ast::eval_name::EvalName) {}
    fn visit_qualifier(&mut self, _: &'a dracaena::ast::qualifier::Qualifier) {}
    fn visit_if_expr(&mut self, _: &'a dracaena::ast::expression::IfExpression) {}
    fn visit_ternary_expr(&mut self, _: &'a dracaena::ast::expression::TernaryExpression) {}
    fn visit_logical_op(&mut self, _: &'a dracaena::ast::operator::LogicalOperator) {}
    fn visit_comparison_op(&mut self, _: &'a dracaena::ast::operator::ComparisonOperator) {}
    fn visit_field_list(&mut self, _: &'a dracaena::ast::field_column::FieldList) {}

    fn visit_dcl_stmt(&mut self, node: &'a dracaena::ast::declaration::DeclarationStmt) {
        match &node.decl_init_list {
            DeclarationInitList::DeclarationInit(DeclarationInit {
                declaration:
                    Declaration {
                        decl_type: DeclarationType::TypeId(type_id),
                        id,
                    },
                assignment_clause: None,
            }) => {
                self.decl_stmts_formatted
                    .push(DeclarationStmtFormatted::new(
                        type_id.to_string(),
                        id.to_string(),
                    ));
            }
            _ => todo!(),
        }
    }

    fn visit_dcl_init_list(&mut self, _: &'a DeclarationInitList) {}
    fn visit_dcl_comma_list(&mut self, _: &'a dracaena::ast::declaration::DeclarationCommaList) {}
    fn visit_dcl_init(&mut self, _: &'a dracaena::ast::declaration::DeclarationInit) {}
    fn visit_dcl(&mut self, _: &'a dracaena::ast::declaration::Declaration) {}
    fn visit_assign_clause(&mut self, _: &'a dracaena::ast::assignment::AssignmentClause) {}
    fn visit_assign_stmt(&mut self, node: &'a AssignmentStmt) {}
    fn visit_id_with_assign_clause(
        &mut self,
        _: &'a dracaena::ast::declaration::IdWithAssigmentClause,
    ) {
    }
    fn visit_assign(&mut self, _: &'a dracaena::ast::assignment::Assign) {}
    fn visit_pre_post_inc_dec_assign(
        &mut self,
        _: &'a dracaena::ast::assignment::PrePostIncDecAssign,
    ) {
    }
    fn visit_assign_inc_dec(&mut self, _: &'a dracaena::ast::assignment::AssignIncDec) {}
    fn visit_stmt(&mut self, _: &'a dracaena::ast::statement::Statement) {}
    fn visit_expr_stmt(&mut self, _: &'a dracaena::ast::statement::ExpressionStatement) {}
    fn visit_local_body(&mut self, node: &'a dracaena::ast::local_body::LocalBody) {
        self.num_decl_stmts = node.dcl_list.len();
        self.num_stmts = node.stmt_list.len();
    }
}

impl<'a> VisitorEnterLeave<'a> for PrettyPrinterVisitor {
    fn leave_visit_local_body(&mut self, node: &'a LocalBody) -> bool {
        let decl_stmts_formatted = std::mem::take(&mut self.decl_stmts_formatted);
        self.source.push_str(
            decl_stmts_formatted
                .into_iter()
                .map(|dcl_stmt_fmt| {
                    format!(
                        "{}{}{};\n",
                        dcl_stmt_fmt.type_id, dcl_stmt_fmt.padding, dcl_stmt_fmt.id
                    )
                })
                .fold(String::new(), |mut acc, str_fmt| {
                    acc.push_str(str_fmt.as_str());
                    acc
                })
                .as_str(),
        );
        let stmts_formatted = std::mem::take(&mut self.stmts_formatted);
        let stmts_formatted = stmts_formatted
            .into_iter()
            .map(|stmt_fmt| match stmt_fmt {
                StatementFormatted::AssignmentStmt(assign_stmt_fmt) => {
                    format!(
                        "{} {} {};\n",
                        assign_stmt_fmt.field, assign_stmt_fmt.assign, assign_stmt_fmt.tern_expr
                    )
                }
                StatementFormatted::Eval(eval_stmt_fmt) => {
                    format!("{}{};\n", eval_stmt_fmt.indent, eval_stmt_fmt.tern_expr)
                }
                StatementFormatted::Function(func_fmt) => {
                    format!("{}{};\n", func_fmt.indent, func_fmt.tern_expr)
                }
            })
            .fold(String::new(), |mut acc, str_fmt| {
                acc.push_str(str_fmt.as_str());
                acc
            });
        self.source.push_str(stmts_formatted.as_str());
        true
    }

    fn leave_visit_dcl_stmt(&mut self, node: &'a DeclarationStmt) -> bool {
        self.current_decl_stmt_idx += 1;
        if self.current_decl_stmt_idx == self.num_decl_stmts {
            // we are at our last decl statement, so we can compute the correct formatting
            if let Some(max_type_len) = self
                .decl_stmts_formatted
                .iter()
                .map(|dcl_stmt| dcl_stmt.type_id.len())
                .max()
            {
                let options = &self.options;
                self.decl_stmts_formatted.iter_mut().for_each(|dcl_stmt| {
                    dcl_stmt.padding = options
                        .indent_str
                        // + 1, because we want to have at least one `indent_str` between `type_id` and `id`
                        .repeat((max_type_len - dcl_stmt.type_id.len()) + 1);
                });
            }
        }
        true
    }

    fn leave_visit_stmt(&mut self, _node: &'a Statement) -> bool {
        let complete_tern_expr_stmt_formatted =
            std::mem::take(&mut self.complete_tern_expr_stmt_formatted);
        if let Some(mut stmt_formatted) = self.current_stmt_formatted.take() {
            match stmt_formatted {
                StatementFormatted::AssignmentStmt(ref mut assign_stmt) => {
                    assign_stmt.tern_expr = complete_tern_expr_stmt_formatted;
                }
                StatementFormatted::Eval(ref mut eval) => {
                    eval.tern_expr = complete_tern_expr_stmt_formatted;
                }
                StatementFormatted::Function(ref mut func) => {
                    func.tern_expr = complete_tern_expr_stmt_formatted;
                }
            }
            self.stmts_formatted.push(stmt_formatted);
        }

        true
    }

    fn enter_visit_expr_stmt(&mut self, node: &'a ExpressionStatement) -> bool {
        match node {
            ExpressionStatement::Eval(_eval) => {
                self.current_stmt_formatted =
                    Some(StatementFormatted::Eval(EvalFormatted::new(String::new())));
                true
            }
            ExpressionStatement::Function(_func) => {
                self.current_stmt_formatted = Some(StatementFormatted::Function(
                    FunctionFormatted::new(String::new()),
                ));
                true
            }
            _ => true,
        }
    }

    fn enter_visit_assign_stmt(&mut self, node: &'a AssignmentStmt) -> bool {
        match node {
            AssignmentStmt::FieldAssign {
                field: Field::Id(field_id),
                assign: Assign::AssignEqual,
                tern_expr: _tern_expr,
            } => {
                self.current_stmt_formatted = Some(StatementFormatted::AssignmentStmt(
                    AssignmentStmtFormatted::new(
                        field_id.to_string(),
                        "=".to_string(),
                        String::new(),
                    ),
                ));
            }
            _ => todo!(),
        }
        true
    }

    fn enter_visit_eval(&mut self, node: &'a Eval) -> bool {
        match (&node.eval_name, &node.parm_list) {
            (EvalName::New(class_id), parm_list) => {
                self.complete_tern_expr_stmt_formatted.push_str("new ");
                self.complete_tern_expr_stmt_formatted
                    .push_str(class_id.as_str());
                self.complete_tern_expr_stmt_formatted.push('(');
                if let Some(parm_list) = parm_list.as_ref() {
                    for (i, parm) in parm_list.iter().enumerate() {
                        parm.take_visitor_enter_leave(self);
                        if i < parm_list.len() - 1 {
                            self.complete_tern_expr_stmt_formatted.push_str(", ");
                        }
                    }
                }
                self.complete_tern_expr_stmt_formatted.push(')');
                return false;
            }
            (EvalName::Qualifier(Qualifier::Id(quali_id), id), parm_list) => {
                self.complete_tern_expr_stmt_formatted
                    .push_str(format!("{}.{}(", quali_id, id).as_str());
                if let Some(parm_list) = parm_list.as_ref() {
                    for (i, parm) in parm_list.iter().enumerate() {
                        parm.take_visitor_enter_leave(self);
                        if i < parm_list.len() - 1 {
                            self.complete_tern_expr_stmt_formatted.push_str(", ");
                        }
                    }
                }
                self.complete_tern_expr_stmt_formatted.push(')');
                return false;
            }
            (EvalName::Qualifier(Qualifier::Eval(eval), id), parm_list) => {
                eval.take_visitor_enter_leave(self);
                self.complete_tern_expr_stmt_formatted
                    .push_str(format!(".{}(", id.as_str()).as_str());
                if let Some(parm_list) = parm_list.as_ref() {
                    for (i, parm) in parm_list.iter().enumerate() {
                        parm.take_visitor_enter_leave(self);
                        if i < parm_list.len() - 1 {
                            self.complete_tern_expr_stmt_formatted.push_str(", ");
                        }
                    }
                }
                self.complete_tern_expr_stmt_formatted.push(')');
                return false;
            }
            _ => {
                // TODO
            }
        }
        true
    }

    fn enter_visit_factor(&mut self, node: &'a Factor) -> bool {
        match node {
            Factor::Field(field) => match field {
                Field::Id(field_id) => {
                    self.complete_tern_expr_stmt_formatted.push_str(field_id);
                    true
                }
                Field::QualifierId(quali, id) => {
                    let mut should_continue = true;
                    match quali {
                        Qualifier::Id(quali_id) => {
                            self.complete_tern_expr_stmt_formatted.push_str(quali_id);
                            self.complete_tern_expr_stmt_formatted.push('.');
                        }
                        Qualifier::Eval(eval) => {
                            eval.take_visitor_enter_leave(self);
                            should_continue = false;
                        }
                    }
                    self.complete_tern_expr_stmt_formatted.push_str(id);
                    should_continue
                }
            },
            Factor::Intrinsics(intrinsics) => {
                self.complete_tern_expr_stmt_formatted
                    .push_str(intrinsics.to_string().as_str());
                true
            }
            Factor::Constant(constant) => {
                self.complete_tern_expr_stmt_formatted
                    .push_str(constant.to_string().as_str());
                true
            }
            _ => true,
        }
    }

    fn enter_visit_function(&mut self, node: &'a Function) -> bool {
        match node {
            Function::StrFmt(parm_elems) => {
                self.complete_tern_expr_stmt_formatted.push_str("strFmt(");
                for (i, parm_elem) in parm_elems.iter().enumerate() {
                    parm_elem.take_visitor_enter_leave(self);
                    if i < parm_elems.len() - 1 {
                        self.complete_tern_expr_stmt_formatted.push_str(", ");
                    }
                }
                self.complete_tern_expr_stmt_formatted.push(')');
                false
            }
            Function::Any2Int(_) => todo!("any2Int not implemented yet"),
            Function::QueryValue(parm_elem) => {
                self.complete_tern_expr_stmt_formatted
                    .push_str("queryValue(");
                parm_elem.take_visitor_enter_leave(self);
                self.complete_tern_expr_stmt_formatted.push(')');
                false
            }
        }
    }
}

impl PrettyPrinterVisitor {
    pub fn new(options: PrettyPrintOptions) -> Self {
        Self {
            options,
            ..Default::default()
        }
    }

    pub fn into_source(self) -> String {
        self.source
    }
}

#[derive(PartialEq)]
struct DeclarationStmtFormatted {
    type_id: String,
    padding: String,
    id: String,
}

impl DeclarationStmtFormatted {
    pub fn new(type_id: String, id: String) -> Self {
        Self {
            type_id,
            padding: "".into(),
            id,
        }
    }
}

#[derive(PartialEq)]
enum StatementFormatted {
    AssignmentStmt(AssignmentStmtFormatted),
    Eval(EvalFormatted),
    Function(FunctionFormatted),
}

#[derive(PartialEq)]
struct AssignmentStmtFormatted {
    field: String,
    padding_assign_left_side: String,
    assign: String,
    tern_expr: String,
}

impl AssignmentStmtFormatted {
    pub fn new(field: String, assign: String, tern_expr: String) -> Self {
        Self {
            field,
            padding_assign_left_side: "".into(),
            assign,
            tern_expr,
        }
    }
}

#[derive(PartialEq)]
struct EvalFormatted {
    indent: String,
    tern_expr: String,
}

impl EvalFormatted {
    pub fn new(tern_expr: String) -> Self {
        Self {
            tern_expr,
            indent: "".into(),
        }
    }
}

#[derive(PartialEq)]
struct FunctionFormatted {
    indent: String,
    tern_expr: String,
}

impl FunctionFormatted {
    pub fn new(tern_expr: String) -> Self {
        Self {
            tern_expr,
            indent: "".into(),
        }
    }
}

#[derive(PartialEq)]
pub struct PrettyPrintOptions {
    indent_str: String,
    indent_depth: usize,
    // maybe we don't need that
    indent_initial: usize,
}

impl Default for PrettyPrintOptions {
    fn default() -> Self {
        Self {
            indent_str: " ".to_string(),
            indent_depth: 4,
            indent_initial: 0,
        }
    }
}

#[cfg(test)]
mod tests {

    use super::*;
    use super::{constant::*, parm::*};
    use dracaena::ast::function::Function;
    use dracaena::ast::intrinsics::Intrinsics;
    use indoc::*;
    use pretty_assertions::assert_eq;

    #[test]
    fn pretty_print_declarations_without_assignments_single() {
        let ast = LocalBody {
            dcl_list: vec![DeclarationStmt {
                decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                    declaration: Declaration {
                        decl_type: DeclarationType::TypeId("TypeFoo".to_owned()),
                        id: "foo".to_owned(),
                    },
                    assignment_clause: None,
                }),
            }],
            stmt_list: vec![],
        };

        let mut pretty_printer_visitor: PrettyPrinterVisitor = Default::default();

        ast.take_visitor_enter_leave(&mut pretty_printer_visitor);

        let expected = indoc! {r#"
            TypeFoo foo;
        "#};

        let actual = pretty_printer_visitor.into_source();

        assert_eq!(expected, actual);
    }

    #[test]
    fn pretty_print_declarations_without_assignments_multiple() {
        let ast = LocalBody {
            dcl_list: vec![
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("TypeFoo".to_owned()),
                            id: "foo".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("TypeBar".to_owned()),
                            id: "bar".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
            ],
            stmt_list: vec![],
        };

        let mut pretty_printer_visitor: PrettyPrinterVisitor = Default::default();

        ast.take_visitor_enter_leave(&mut pretty_printer_visitor);

        let expected = indoc! {r#"
            TypeFoo foo;
            TypeBar bar;
        "#};

        let actual = pretty_printer_visitor.into_source();

        assert_eq!(expected, actual);
    }

    #[test]
    fn pretty_print_declarations_without_assignments_multiple_different_length_of_type() {
        let ast = LocalBody {
            dcl_list: vec![
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("TypeFoo".to_owned()),
                            id: "foo".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("TypeFooBar".to_owned()),
                            id: "bar".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
            ],
            stmt_list: vec![],
        };

        let mut pretty_printer_visitor: PrettyPrinterVisitor = Default::default();

        ast.take_visitor_enter_leave(&mut pretty_printer_visitor);

        let expected = indoc! {r#"
            TypeFoo    foo;
            TypeFooBar bar;
        "#};

        let actual = pretty_printer_visitor.into_source();

        assert_eq!(expected, actual);
    }

    #[test]
    fn pretty_print_statements_assignment_single_simple_new() {
        let ast = LocalBody {
            dcl_list: vec![],
            stmt_list: vec![AssignmentStmt::FieldAssign {
                field: Field::Id("foo".to_owned()),
                assign: Assign::AssignEqual,
                tern_expr: Factor::Eval(Eval {
                    eval_name: EvalName::New("Foo".to_owned()),
                    parm_list: None,
                })
                .builder()
                .into(),
            }
            .into()],
        };

        let mut pretty_printer_visitor: PrettyPrinterVisitor = Default::default();

        ast.take_visitor_enter_leave(&mut pretty_printer_visitor);

        let expected = indoc! {r#"
            foo = new Foo();
        "#};

        let actual = pretty_printer_visitor.into_source();

        assert_eq!(expected, actual);
    }

    #[test]
    fn pretty_print_statements_eval_single_simple_new() {
        let ast = LocalBody {
            dcl_list: vec![],
            stmt_list: vec![Eval {
                eval_name: EvalName::New("Foo".to_owned()),
                parm_list: None,
            }
            .into()],
        };

        let mut pretty_printer_visitor: PrettyPrinterVisitor = Default::default();

        ast.take_visitor_enter_leave(&mut pretty_printer_visitor);

        let expected = indoc! {r#"
            new Foo();
        "#};

        let actual = pretty_printer_visitor.into_source();

        assert_eq!(expected, actual);
    }

    #[test]
    fn pretty_print_statements_assignment_single_new_nested() {
        let ast = LocalBody {
            dcl_list: vec![],
            stmt_list: vec![AssignmentStmt::FieldAssign {
                field: Field::Id("foo".to_owned()),
                assign: Assign::AssignEqual,
                tern_expr: Factor::Eval(Eval {
                    eval_name: EvalName::New("Foo".to_owned()),
                    parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                        Factor::Eval(Eval {
                            eval_name: EvalName::New("Bar".to_string()),
                            parm_list: None,
                        })
                        .builder()
                        .into(),
                    ))]),
                })
                .builder()
                .into(),
            }
            .into()],
        };

        let mut pretty_printer_visitor: PrettyPrinterVisitor = Default::default();

        ast.take_visitor_enter_leave(&mut pretty_printer_visitor);

        let expected = indoc! {r#"
            foo = new Foo(new Bar());
        "#};

        let actual = pretty_printer_visitor.into_source();

        assert_eq!(expected, actual);
    }

    #[test]
    fn pretty_print_statements_eval_single_new_nested() {
        let ast = LocalBody {
            dcl_list: vec![],
            stmt_list: vec![Eval {
                eval_name: EvalName::New("Foo".to_owned()),
                parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                    Factor::Eval(Eval {
                        eval_name: EvalName::New("Bar".to_string()),
                        parm_list: None,
                    })
                    .builder()
                    .into(),
                ))]),
            }
            .into()],
        };

        let mut pretty_printer_visitor: PrettyPrinterVisitor = Default::default();

        ast.take_visitor_enter_leave(&mut pretty_printer_visitor);

        let expected = indoc! {r#"
            new Foo(new Bar());
        "#};

        let actual = pretty_printer_visitor.into_source();

        assert_eq!(expected, actual);
    }

    #[test]
    fn pretty_print_statements_assignment_single_simple_qualified_method_call() {
        let ast = LocalBody {
            dcl_list: vec![],
            stmt_list: vec![AssignmentStmt::FieldAssign {
                field: Field::Id("foo".to_owned()),
                assign: Assign::AssignEqual,
                tern_expr: Factor::Eval(Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("bar".to_string()),
                        "baz".to_string(),
                    ),
                    parm_list: None,
                })
                .builder()
                .into(),
            }
            .into()],
        };

        let mut pretty_printer_visitor: PrettyPrinterVisitor = Default::default();

        ast.take_visitor_enter_leave(&mut pretty_printer_visitor);

        let expected = indoc! {r#"
            foo = bar.baz();
        "#};

        let actual = pretty_printer_visitor.into_source();

        assert_eq!(expected, actual);
    }

    #[test]
    fn pretty_print_statements_eval_single_simple_qualified_method_call() {
        let ast = LocalBody {
            dcl_list: vec![],
            stmt_list: vec![Eval {
                eval_name: EvalName::Qualifier(Qualifier::Id("foo".into()), "bar".into()),
                parm_list: None,
            }
            .into()],
        };

        let mut pretty_printer_visitor: PrettyPrinterVisitor = Default::default();

        ast.take_visitor_enter_leave(&mut pretty_printer_visitor);

        let expected = indoc! {r#"
            foo.bar();
        "#};

        let actual = pretty_printer_visitor.into_source();

        assert_eq!(expected, actual);
    }

    #[test]
    fn pretty_print_statements_assignment_single_qualified_method_call_nested() {
        let ast = LocalBody {
            dcl_list: vec![],
            stmt_list: vec![AssignmentStmt::FieldAssign {
                field: Field::Id("foo".to_owned()),
                assign: Assign::AssignEqual,
                tern_expr: Factor::Eval(Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("bar".to_string()),
                        "baz".to_string(),
                    ),
                    parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                        Factor::Eval(Eval {
                            eval_name: EvalName::Qualifier(
                                Qualifier::Id("qux".to_string()),
                                "quux".to_string(),
                            ),
                            parm_list: None,
                        })
                        .builder()
                        .into(),
                    ))]),
                })
                .builder()
                .into(),
            }
            .into()],
        };

        let mut pretty_printer_visitor: PrettyPrinterVisitor = Default::default();

        ast.take_visitor_enter_leave(&mut pretty_printer_visitor);

        let expected = indoc! {r#"
            foo = bar.baz(qux.quux());
        "#};

        let actual = pretty_printer_visitor.into_source();

        assert_eq!(expected, actual);
    }

    #[test]
    fn pretty_print_statements_eval_single_qualified_method_call_nested() {
        let ast = LocalBody {
            dcl_list: vec![],
            stmt_list: vec![Eval {
                eval_name: EvalName::Qualifier(Qualifier::Id("bar".to_string()), "baz".to_string()),
                parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                    Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("qux".to_string()),
                            "quux".to_string(),
                        ),
                        parm_list: None,
                    })
                    .builder()
                    .into(),
                ))]),
            }
            .into()],
        };

        let mut pretty_printer_visitor: PrettyPrinterVisitor = Default::default();

        ast.take_visitor_enter_leave(&mut pretty_printer_visitor);

        let expected = indoc! {r#"
            bar.baz(qux.quux());
        "#};

        let actual = pretty_printer_visitor.into_source();

        assert_eq!(expected, actual);
    }

    #[test]
    fn pretty_print_statements_assignment_single_qualified_method_call_nested_with_field_id() {
        let ast = LocalBody {
            dcl_list: vec![],
            stmt_list: vec![AssignmentStmt::FieldAssign {
                field: Field::Id("foo".to_owned()),
                assign: Assign::AssignEqual,
                tern_expr: Factor::Eval(Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("bar".to_string()),
                        "baz".to_string(),
                    ),
                    parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                        Factor::Field(Field::Id("qux".to_string())).builder().into(),
                    ))]),
                })
                .builder()
                .into(),
            }
            .into()],
        };

        let mut pretty_printer_visitor: PrettyPrinterVisitor = Default::default();

        ast.take_visitor_enter_leave(&mut pretty_printer_visitor);

        let expected = indoc! {r#"
            foo = bar.baz(qux);
        "#};

        let actual = pretty_printer_visitor.into_source();

        assert_eq!(expected, actual);
    }

    #[test]
    fn pretty_print_statements_eval_single_qualified_method_call_nested_with_field_id() {
        let ast = LocalBody {
            dcl_list: vec![],
            stmt_list: vec![Eval {
                eval_name: EvalName::Qualifier(Qualifier::Id("bar".to_string()), "baz".to_string()),
                parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                    Factor::Field(Field::Id("qux".to_string())).builder().into(),
                ))]),
            }
            .into()],
        };

        let mut pretty_printer_visitor: PrettyPrinterVisitor = Default::default();

        ast.take_visitor_enter_leave(&mut pretty_printer_visitor);

        let expected = indoc! {r#"
            bar.baz(qux);
        "#};

        let actual = pretty_printer_visitor.into_source();

        assert_eq!(expected, actual);
    }

    #[test]
    fn pretty_print_statements_assignment_single_qualified_method_call_nested_with_field_qualifier_id(
    ) {
        let ast = LocalBody {
            dcl_list: vec![],
            stmt_list: vec![AssignmentStmt::FieldAssign {
                field: Field::Id("foo".to_owned()),
                assign: Assign::AssignEqual,
                tern_expr: Factor::Eval(Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("bar".to_string()),
                        "baz".to_string(),
                    ),
                    parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                        Factor::Field(Field::QualifierId(
                            Qualifier::Id("qux".to_string()),
                            "quux".to_string(),
                        ))
                        .builder()
                        .into(),
                    ))]),
                })
                .builder()
                .into(),
            }
            .into()],
        };

        let mut pretty_printer_visitor: PrettyPrinterVisitor = Default::default();

        ast.take_visitor_enter_leave(&mut pretty_printer_visitor);

        let expected = indoc! {r#"
            foo = bar.baz(qux.quux);
        "#};

        let actual = pretty_printer_visitor.into_source();

        assert_eq!(expected, actual);
    }

    #[test]
    fn pretty_print_statements_eval_single_qualified_method_call_nested_with_field_qualifier_id() {
        let ast = LocalBody {
            dcl_list: vec![],
            stmt_list: vec![Eval {
                eval_name: EvalName::Qualifier(Qualifier::Id("bar".to_string()), "baz".to_string()),
                parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                    Factor::Field(Field::QualifierId(
                        Qualifier::Id("qux".to_string()),
                        "quux".to_string(),
                    ))
                    .builder()
                    .into(),
                ))]),
            }
            .into()],
        };

        let mut pretty_printer_visitor: PrettyPrinterVisitor = Default::default();

        ast.take_visitor_enter_leave(&mut pretty_printer_visitor);

        let expected = indoc! {r#"
            bar.baz(qux.quux);
        "#};

        let actual = pretty_printer_visitor.into_source();

        assert_eq!(expected, actual);
    }

    #[test]
    fn pretty_print_statements_assignment_single_qualified_method_call_nested_with_intrinsic_field_num(
    ) {
        let ast = LocalBody {
            dcl_list: vec![],
            stmt_list: vec![AssignmentStmt::FieldAssign {
                field: Field::Id("foo".to_owned()),
                assign: Assign::AssignEqual,
                tern_expr: Factor::Eval(Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("bar".to_string()),
                        "baz".to_string(),
                    ),
                    parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                        Factor::Intrinsics(Intrinsics::FieldNum(
                            "CustTable".into(),
                            "AccountNum".into(),
                        ))
                        .builder()
                        .into(),
                    ))]),
                })
                .builder()
                .into(),
            }
            .into()],
        };

        let mut pretty_printer_visitor: PrettyPrinterVisitor = Default::default();

        ast.take_visitor_enter_leave(&mut pretty_printer_visitor);

        let expected = indoc! {r#"
            foo = bar.baz(fieldNum(CustTable, AccountNum));
        "#};

        let actual = pretty_printer_visitor.into_source();

        assert_eq!(expected, actual);
    }

    #[test]
    fn pretty_print_statements_eval_single_qualified_method_call_nested_with_intrinsic_field_num() {
        let ast = LocalBody {
            dcl_list: vec![],
            stmt_list: vec![Eval {
                eval_name: EvalName::Qualifier(Qualifier::Id("bar".to_string()), "baz".to_string()),
                parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                    Factor::Intrinsics(Intrinsics::FieldNum(
                        "CustTable".into(),
                        "AccountNum".into(),
                    ))
                    .builder()
                    .into(),
                ))]),
            }
            .into()],
        };

        let mut pretty_printer_visitor: PrettyPrinterVisitor = Default::default();

        ast.take_visitor_enter_leave(&mut pretty_printer_visitor);

        let expected = indoc! {r#"
            bar.baz(fieldNum(CustTable, AccountNum));
        "#};

        let actual = pretty_printer_visitor.into_source();

        assert_eq!(expected, actual);
    }

    #[test]
    fn pretty_print_statements_assignment_single_qualified_method_call_nested_with_intrinsic_field_str(
    ) {
        let ast = LocalBody {
            dcl_list: vec![],
            stmt_list: vec![AssignmentStmt::FieldAssign {
                field: Field::Id("foo".to_owned()),
                assign: Assign::AssignEqual,
                tern_expr: Factor::Eval(Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("bar".to_string()),
                        "baz".to_string(),
                    ),
                    parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                        Factor::Intrinsics(Intrinsics::FieldStr(
                            "CustTable".into(),
                            "AccountNum".into(),
                        ))
                        .builder()
                        .into(),
                    ))]),
                })
                .builder()
                .into(),
            }
            .into()],
        };

        let mut pretty_printer_visitor: PrettyPrinterVisitor = Default::default();

        ast.take_visitor_enter_leave(&mut pretty_printer_visitor);

        let expected = indoc! {r#"
            foo = bar.baz(fieldStr(CustTable, AccountNum));
        "#};

        let actual = pretty_printer_visitor.into_source();

        assert_eq!(expected, actual);
    }

    #[test]
    fn pretty_print_statements_eval_single_qualified_method_call_nested_with_intrinsic_field_str() {
        let ast = LocalBody {
            dcl_list: vec![],
            stmt_list: vec![Eval {
                eval_name: EvalName::Qualifier(Qualifier::Id("bar".to_string()), "baz".to_string()),
                parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                    Factor::Intrinsics(Intrinsics::FieldStr(
                        "CustTable".into(),
                        "AccountNum".into(),
                    ))
                    .builder()
                    .into(),
                ))]),
            }
            .into()],
        };

        let mut pretty_printer_visitor: PrettyPrinterVisitor = Default::default();

        ast.take_visitor_enter_leave(&mut pretty_printer_visitor);

        let expected = indoc! {r#"
            bar.baz(fieldStr(CustTable, AccountNum));
        "#};

        let actual = pretty_printer_visitor.into_source();

        assert_eq!(expected, actual);
    }

    #[test]
    fn pretty_print_statements_assignment_single_qualified_method_call_nested_with_intrinsic_table_num(
    ) {
        let ast = LocalBody {
            dcl_list: vec![],
            stmt_list: vec![AssignmentStmt::FieldAssign {
                field: Field::Id("foo".to_owned()),
                assign: Assign::AssignEqual,
                tern_expr: Factor::Eval(Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("bar".to_string()),
                        "baz".to_string(),
                    ),
                    parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                        Factor::Intrinsics(Intrinsics::TableNum("CustTable".into()))
                            .builder()
                            .into(),
                    ))]),
                })
                .builder()
                .into(),
            }
            .into()],
        };

        let mut pretty_printer_visitor: PrettyPrinterVisitor = Default::default();

        ast.take_visitor_enter_leave(&mut pretty_printer_visitor);

        let expected = indoc! {r#"
            foo = bar.baz(tableNum(CustTable));
        "#};

        let actual = pretty_printer_visitor.into_source();

        assert_eq!(expected, actual);
    }

    #[test]
    fn pretty_print_statements_eval_single_qualified_method_call_nested_with_intrinsic_table_num() {
        let ast = LocalBody {
            dcl_list: vec![],
            stmt_list: vec![Eval {
                eval_name: EvalName::Qualifier(Qualifier::Id("bar".to_string()), "baz".to_string()),
                parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                    Factor::Intrinsics(Intrinsics::TableNum("CustTable".into()))
                        .builder()
                        .into(),
                ))]),
            }
            .into()],
        };

        let mut pretty_printer_visitor: PrettyPrinterVisitor = Default::default();

        ast.take_visitor_enter_leave(&mut pretty_printer_visitor);

        let expected = indoc! {r#"
            bar.baz(tableNum(CustTable));
        "#};

        let actual = pretty_printer_visitor.into_source();

        assert_eq!(expected, actual);
    }

    #[test]
    fn pretty_print_statements_assignment_single_qualified_method_call_nested_with_constants() {
        let ast = LocalBody {
            dcl_list: vec![],
            stmt_list: vec![
                AssignmentStmt::FieldAssign {
                    field: Field::Id("foo".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("bar".to_string()),
                            "baz".to_string(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Constant(Constant::Null).builder().into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("foo".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("bar".to_string()),
                            "baz".to_string(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Constant(Constant::True).builder().into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("foo".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("bar".to_string()),
                            "baz".to_string(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Constant(Constant::False).builder().into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("foo".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("bar".to_string()),
                            "baz".to_string(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Constant(Constant::Integer(123)).builder().into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("foo".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("bar".to_string()),
                            "baz".to_string(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Constant(Constant::Integer64(123)).builder().into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("foo".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("bar".to_string()),
                            "baz".to_string(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Constant(Constant::Double(123.45)).builder().into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("foo".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("bar".to_string()),
                            "baz".to_string(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Constant(Constant::Enum("State".into(), "Active".into()))
                                .builder()
                                .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
            ],
        };

        let mut pretty_printer_visitor: PrettyPrinterVisitor = Default::default();

        ast.take_visitor_enter_leave(&mut pretty_printer_visitor);

        let expected = indoc! {r#"
            foo = bar.baz(null);
            foo = bar.baz(true);
            foo = bar.baz(false);
            foo = bar.baz(123);
            foo = bar.baz(123u);
            foo = bar.baz(123.45);
            foo = bar.baz(State::Active);
        "#};

        let actual = pretty_printer_visitor.into_source();

        assert_eq!(expected, actual);
    }

    #[test]
    fn pretty_print_statements_eval_single_qualified_method_call_nested_with_constants() {
        let ast = LocalBody {
            dcl_list: vec![],
            stmt_list: vec![
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("bar".to_string()),
                        "baz".to_string(),
                    ),
                    parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                        Factor::Constant(Constant::Null).builder().into(),
                    ))]),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("bar".to_string()),
                        "baz".to_string(),
                    ),
                    parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                        Factor::Constant(Constant::True).builder().into(),
                    ))]),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("bar".to_string()),
                        "baz".to_string(),
                    ),
                    parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                        Factor::Constant(Constant::False).builder().into(),
                    ))]),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("bar".to_string()),
                        "baz".to_string(),
                    ),
                    parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                        Factor::Constant(Constant::Integer(123)).builder().into(),
                    ))]),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("bar".to_string()),
                        "baz".to_string(),
                    ),
                    parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                        Factor::Constant(Constant::Integer64(123)).builder().into(),
                    ))]),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("bar".to_string()),
                        "baz".to_string(),
                    ),
                    parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                        Factor::Constant(Constant::Double(123.45)).builder().into(),
                    ))]),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("bar".to_string()),
                        "baz".to_string(),
                    ),
                    parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                        Factor::Constant(Constant::Enum("State".into(), "Active".into()))
                            .builder()
                            .into(),
                    ))]),
                }
                .into(),
            ],
        };

        let mut pretty_printer_visitor: PrettyPrinterVisitor = Default::default();

        ast.take_visitor_enter_leave(&mut pretty_printer_visitor);

        let expected = indoc! {r#"
            bar.baz(null);
            bar.baz(true);
            bar.baz(false);
            bar.baz(123);
            bar.baz(123u);
            bar.baz(123.45);
            bar.baz(State::Active);
        "#};

        let actual = pretty_printer_visitor.into_source();

        assert_eq!(expected, actual);
    }

    #[test]
    fn pretty_print_statements_assignment_multiple_mixed() {
        let ast = LocalBody {
            dcl_list: vec![],
            stmt_list: vec![
                AssignmentStmt::FieldAssign {
                    field: Field::Id("foo".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("bar".to_string()),
                            "baz".to_string(),
                        ),
                        parm_list: None,
                    })
                    .builder()
                    .into(),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("foo".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::New("Foo".to_owned()),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Constant(Constant::Enum("State".into(), "Active".into()))
                                .builder()
                                .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
            ],
        };

        let mut pretty_printer_visitor: PrettyPrinterVisitor = Default::default();

        ast.take_visitor_enter_leave(&mut pretty_printer_visitor);

        let expected = indoc! {r#"
            foo = bar.baz();
            foo = new Foo(State::Active);
        "#};

        let actual = pretty_printer_visitor.into_source();

        assert_eq!(expected, actual);
    }

    #[test]
    fn pretty_print_statements_eval_multiple_mixed() {
        let ast = LocalBody {
            dcl_list: vec![],
            stmt_list: vec![
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("bar".to_string()),
                        "baz".to_string(),
                    ),
                    parm_list: None,
                }
                .into(),
                Eval {
                    eval_name: EvalName::New("Foo".to_owned()),
                    parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                        Factor::Constant(Constant::Enum("State".into(), "Active".into()))
                            .builder()
                            .into(),
                    ))]),
                }
                .into(),
            ],
        };

        let mut pretty_printer_visitor: PrettyPrinterVisitor = Default::default();

        ast.take_visitor_enter_leave(&mut pretty_printer_visitor);

        let expected = indoc! {r#"
            bar.baz();
            new Foo(State::Active);
        "#};

        let actual = pretty_printer_visitor.into_source();

        assert_eq!(expected, actual);
    }

    #[test]
    fn pretty_print_statements_assignment_single_qualified_method_call_nested_with_two_params() {
        let ast = LocalBody {
            dcl_list: vec![],
            stmt_list: vec![AssignmentStmt::FieldAssign {
                field: Field::Id("foo".to_owned()),
                assign: Assign::AssignEqual,
                tern_expr: Factor::Eval(Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("bar".to_string()),
                        "baz".to_string(),
                    ),
                    parm_list: Some(vec![
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::Id("quux".into())).builder().into(),
                        )),
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Intrinsics(Intrinsics::TableNum("CustTable".into()))
                                .builder()
                                .into(),
                        )),
                    ]),
                })
                .builder()
                .into(),
            }
            .into()],
        };

        let mut pretty_printer_visitor: PrettyPrinterVisitor = Default::default();

        ast.take_visitor_enter_leave(&mut pretty_printer_visitor);

        let expected = indoc! {r#"
            foo = bar.baz(quux, tableNum(CustTable));
        "#};

        let actual = pretty_printer_visitor.into_source();

        assert_eq!(expected, actual);
    }

    #[test]
    fn pretty_print_statements_eval_single_qualified_method_call_nested_with_two_params() {
        let ast = LocalBody {
            dcl_list: vec![],
            stmt_list: vec![Eval {
                eval_name: EvalName::Qualifier(Qualifier::Id("bar".to_string()), "baz".to_string()),
                parm_list: Some(vec![
                    ParmElem::TernaryExpression(Box::new(
                        Factor::Field(Field::Id("quux".into())).builder().into(),
                    )),
                    ParmElem::TernaryExpression(Box::new(
                        Factor::Intrinsics(Intrinsics::TableNum("CustTable".into()))
                            .builder()
                            .into(),
                    )),
                ]),
            }
            .into()],
        };

        let mut pretty_printer_visitor: PrettyPrinterVisitor = Default::default();

        ast.take_visitor_enter_leave(&mut pretty_printer_visitor);

        let expected = indoc! {r#"
            bar.baz(quux, tableNum(CustTable));
        "#};

        let actual = pretty_printer_visitor.into_source();

        assert_eq!(expected, actual);
    }

    #[test]
    fn pretty_print_statements_assignment_single_chained_qualified_method_call_nested_with_two_params(
    ) {
        let ast = LocalBody {
            dcl_list: vec![],
            stmt_list: vec![AssignmentStmt::FieldAssign {
                field: Field::Id("foo".to_owned()),
                assign: Assign::AssignEqual,
                tern_expr: Factor::Eval(Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Eval(Box::new(Eval {
                            eval_name: EvalName::Qualifier(
                                Qualifier::Id("bar".to_string()),
                                "corge".to_string(),
                            ),
                            parm_list: Some(vec![
                                ParmElem::TernaryExpression(Box::new(
                                    Factor::Field(Field::Id("quux".into())).builder().into(),
                                )),
                                ParmElem::TernaryExpression(Box::new(
                                    Factor::Intrinsics(Intrinsics::TableNum("CustTable".into()))
                                        .builder()
                                        .into(),
                                )),
                            ]),
                        })),
                        "baz".to_string(),
                    ),
                    parm_list: Some(vec![
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::Id("grault".into())).builder().into(),
                        )),
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Eval(Eval {
                                eval_name: EvalName::New("Drix".to_string()),
                                parm_list: Some(vec![
                                    ParmElem::TernaryExpression(Box::new(
                                        Factor::Constant(Constant::Null).builder().into(),
                                    )),
                                    ParmElem::TernaryExpression(Box::new(
                                        Factor::Constant(Constant::True).builder().into(),
                                    )),
                                ]),
                            })
                            .builder()
                            .into(),
                        )),
                    ]),
                })
                .builder()
                .into(),
            }
            .into()],
        };

        let mut pretty_printer_visitor: PrettyPrinterVisitor = Default::default();

        ast.take_visitor_enter_leave(&mut pretty_printer_visitor);

        let expected = indoc! {r#"
            foo = bar.corge(quux, tableNum(CustTable)).baz(grault, new Drix(null, true));
        "#};

        let actual = pretty_printer_visitor.into_source();

        assert_eq!(expected, actual);
    }

    #[test]
    fn pretty_print_statements_eval_single_chained_qualified_method_call_nested_with_two_params() {
        let ast = LocalBody {
            dcl_list: vec![],
            stmt_list: vec![Eval {
                eval_name: EvalName::Qualifier(
                    Qualifier::Eval(Box::new(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("bar".to_string()),
                            "corge".to_string(),
                        ),
                        parm_list: Some(vec![
                            ParmElem::TernaryExpression(Box::new(
                                Factor::Field(Field::Id("quux".into())).builder().into(),
                            )),
                            ParmElem::TernaryExpression(Box::new(
                                Factor::Intrinsics(Intrinsics::TableNum("CustTable".into()))
                                    .builder()
                                    .into(),
                            )),
                        ]),
                    })),
                    "baz".to_string(),
                ),
                parm_list: Some(vec![
                    ParmElem::TernaryExpression(Box::new(
                        Factor::Field(Field::Id("grault".into())).builder().into(),
                    )),
                    ParmElem::TernaryExpression(Box::new(
                        Factor::Eval(Eval {
                            eval_name: EvalName::New("Drix".to_string()),
                            parm_list: Some(vec![
                                ParmElem::TernaryExpression(Box::new(
                                    Factor::Constant(Constant::Null).builder().into(),
                                )),
                                ParmElem::TernaryExpression(Box::new(
                                    Factor::Constant(Constant::True).builder().into(),
                                )),
                            ]),
                        })
                        .builder()
                        .into(),
                    )),
                ]),
            }
            .into()],
        };

        let mut pretty_printer_visitor: PrettyPrinterVisitor = Default::default();

        ast.take_visitor_enter_leave(&mut pretty_printer_visitor);

        let expected = indoc! {r#"
            bar.corge(quux, tableNum(CustTable)).baz(grault, new Drix(null, true));
        "#};

        let actual = pretty_printer_visitor.into_source();

        assert_eq!(expected, actual);
    }

    #[test]
    fn pretty_print_statements_assignment_single_chained_qualified_method_call_strfmt() {
        let ast = LocalBody {
            dcl_list: vec![],
            stmt_list: vec![AssignmentStmt::FieldAssign {
                field: Field::Id("foo".to_owned()),
                assign: Assign::AssignEqual,
                tern_expr: Factor::Eval(Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Eval(Box::new(Eval {
                            eval_name: EvalName::Qualifier(
                                Qualifier::Id("bar".to_string()),
                                "corge".to_string(),
                            ),
                            parm_list: Some(vec![
                                ParmElem::TernaryExpression(Box::new(
                                    Factor::Field(Field::Id("quux".into())).builder().into(),
                                )),
                                ParmElem::TernaryExpression(Box::new(
                                    Factor::Intrinsics(Intrinsics::TableNum("CustTable".into()))
                                        .builder()
                                        .into(),
                                )),
                            ]),
                        })),
                        "baz".to_string(),
                    ),
                    parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                        Factor::Function(Function::StrFmt(vec![
                            ParmElem::TernaryExpression(Box::new(
                                Factor::Constant(Constant::String("(%1 == %2)".into()))
                                    .builder()
                                    .into(),
                            )),
                            ParmElem::TernaryExpression(Box::new(
                                Factor::Field(Field::Id("AccountNumFoo".into()))
                                    .builder()
                                    .into(),
                            )),
                            ParmElem::TernaryExpression(Box::new(
                                Factor::Field(Field::Id("AccountNumBar".into()))
                                    .builder()
                                    .into(),
                            )),
                        ]))
                        .builder()
                        .into(),
                    ))]),
                })
                .builder()
                .into(),
            }
            .into()],
        };

        let mut pretty_printer_visitor: PrettyPrinterVisitor = Default::default();

        ast.take_visitor_enter_leave(&mut pretty_printer_visitor);

        let expected = indoc! {r#"
            foo = bar.corge(quux, tableNum(CustTable)).baz(strFmt('(%1 == %2)', AccountNumFoo, AccountNumBar));
        "#};

        let actual = pretty_printer_visitor.into_source();

        assert_eq!(expected, actual);
    }

    #[test]
    fn pretty_print_statements_assignment_single_chained_qualified_method_call_strfmt_with_field_str(
    ) {
        let ast = LocalBody {
            dcl_list: vec![],
            stmt_list: vec![AssignmentStmt::FieldAssign {
                field: Field::Id("foo".to_owned()),
                assign: Assign::AssignEqual,
                tern_expr: Factor::Eval(Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Eval(Box::new(Eval {
                            eval_name: EvalName::Qualifier(
                                Qualifier::Id("bar".to_string()),
                                "corge".to_string(),
                            ),
                            parm_list: Some(vec![
                                ParmElem::TernaryExpression(Box::new(
                                    Factor::Field(Field::Id("quux".into())).builder().into(),
                                )),
                                ParmElem::TernaryExpression(Box::new(
                                    Factor::Intrinsics(Intrinsics::TableNum("CustTable".into()))
                                        .builder()
                                        .into(),
                                )),
                            ]),
                        })),
                        "baz".to_string(),
                    ),
                    parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                        Factor::Function(Function::StrFmt(vec![
                            ParmElem::TernaryExpression(Box::new(
                                Factor::Constant(Constant::String(
                                    r#"((%1 == "%2") || (%3 == "%4"))"#.into(),
                                ))
                                .builder()
                                .into(),
                            )),
                            ParmElem::TernaryExpression(Box::new(
                                Factor::Intrinsics(Intrinsics::FieldStr(
                                    "CustTable".into(),
                                    "AccountNum".into(),
                                ))
                                .builder()
                                .into(),
                            )),
                            ParmElem::TernaryExpression(Box::new(
                                Factor::Function(Function::QueryValue(
                                    ParmElem::TernaryExpression(Box::new(
                                        Factor::Constant(Constant::String("4000".into()))
                                            .builder()
                                            .into(),
                                    )),
                                ))
                                .builder()
                                .into(),
                            )),
                            ParmElem::TernaryExpression(Box::new(
                                Factor::Intrinsics(Intrinsics::FieldStr(
                                    "CustTable".into(),
                                    "Name".into(),
                                ))
                                .builder()
                                .into(),
                            )),
                            ParmElem::TernaryExpression(Box::new(
                                Factor::Function(Function::QueryValue(
                                    ParmElem::TernaryExpression(Box::new(
                                        Factor::Constant(Constant::String("Foo".into()))
                                            .builder()
                                            .into(),
                                    )),
                                ))
                                .builder()
                                .into(),
                            )),
                        ]))
                        .builder()
                        .into(),
                    ))]),
                })
                .builder()
                .into(),
            }
            .into()],
        };

        let mut pretty_printer_visitor: PrettyPrinterVisitor = Default::default();

        ast.take_visitor_enter_leave(&mut pretty_printer_visitor);

        let expected = indoc! {r#"
        foo = bar.corge(quux, tableNum(CustTable)).baz(strFmt('((%1 == "%2") || (%3 == "%4"))', fieldStr(CustTable, AccountNum), queryValue('4000'), fieldStr(CustTable, Name), queryValue('Foo')));
        "#};

        let actual = pretty_printer_visitor.into_source();

        assert_eq!(expected, actual);
    }

    #[test]
    fn pretty_print_statements_assignment_single_chained_long_qualified_method_call_nested_with_two_params(
    ) {
        let ast = LocalBody {
            dcl_list: vec![],
            stmt_list: vec![AssignmentStmt::FieldAssign {
                field: Field::Id("foo".to_owned()),
                assign: Assign::AssignEqual,
                tern_expr: Factor::Eval(Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Eval(Box::new(Eval {
                            eval_name: EvalName::Qualifier(
                                Qualifier::Eval(Box::new(Eval {
                                    eval_name: EvalName::Qualifier(
                                        Qualifier::Id("bar".to_string()),
                                        "corge".to_string(),
                                    ),
                                    parm_list: Some(vec![
                                        ParmElem::TernaryExpression(Box::new(
                                            Factor::Field(Field::Id("quux".into()))
                                                .builder()
                                                .into(),
                                        )),
                                        ParmElem::TernaryExpression(Box::new(
                                            Factor::Intrinsics(Intrinsics::TableNum(
                                                "CustTable".into(),
                                            ))
                                            .builder()
                                            .into(),
                                        )),
                                    ]),
                                })),
                                "baz".to_string(),
                            ),
                            parm_list: Some(vec![
                                ParmElem::TernaryExpression(Box::new(
                                    Factor::Field(Field::Id("grault".into())).builder().into(),
                                )),
                                ParmElem::TernaryExpression(Box::new(
                                    Factor::Eval(Eval {
                                        eval_name: EvalName::New("Drix".to_string()),
                                        parm_list: Some(vec![
                                            ParmElem::TernaryExpression(Box::new(
                                                Factor::Constant(Constant::Null).builder().into(),
                                            )),
                                            ParmElem::TernaryExpression(Box::new(
                                                Factor::Constant(Constant::True).builder().into(),
                                            )),
                                        ]),
                                    })
                                    .builder()
                                    .into(),
                                )),
                            ]),
                        })),
                        "done".to_string(),
                    ),
                    parm_list: Some(vec![
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Constant(Constant::True).builder().into(),
                        )),
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Constant(Constant::Integer(42)).builder().into(),
                        )),
                    ]),
                })
                .builder()
                .into(),
            }
            .into()],
        };

        let mut pretty_printer_visitor: PrettyPrinterVisitor = Default::default();

        ast.take_visitor_enter_leave(&mut pretty_printer_visitor);

        let expected = indoc! {r#"
            foo = bar.corge(quux, tableNum(CustTable)).baz(grault, new Drix(null, true)).done(true, 42);
        "#};

        let actual = pretty_printer_visitor.into_source();

        assert_eq!(expected, actual);
    }

    #[test]
    fn pretty_print_statements_eval_single_chained_long_qualified_method_call_nested_with_two_params(
    ) {
        let ast = LocalBody {
            dcl_list: vec![],
            stmt_list: vec![Eval {
                eval_name: EvalName::Qualifier(
                    Qualifier::Eval(Box::new(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Eval(Box::new(Eval {
                                eval_name: EvalName::Qualifier(
                                    Qualifier::Id("bar".to_string()),
                                    "corge".to_string(),
                                ),
                                parm_list: Some(vec![
                                    ParmElem::TernaryExpression(Box::new(
                                        Factor::Field(Field::Id("quux".into())).builder().into(),
                                    )),
                                    ParmElem::TernaryExpression(Box::new(
                                        Factor::Intrinsics(Intrinsics::TableNum(
                                            "CustTable".into(),
                                        ))
                                        .builder()
                                        .into(),
                                    )),
                                ]),
                            })),
                            "baz".to_string(),
                        ),
                        parm_list: Some(vec![
                            ParmElem::TernaryExpression(Box::new(
                                Factor::Field(Field::Id("grault".into())).builder().into(),
                            )),
                            ParmElem::TernaryExpression(Box::new(
                                Factor::Eval(Eval {
                                    eval_name: EvalName::New("Drix".to_string()),
                                    parm_list: Some(vec![
                                        ParmElem::TernaryExpression(Box::new(
                                            Factor::Constant(Constant::Null).builder().into(),
                                        )),
                                        ParmElem::TernaryExpression(Box::new(
                                            Factor::Constant(Constant::True).builder().into(),
                                        )),
                                    ]),
                                })
                                .builder()
                                .into(),
                            )),
                        ]),
                    })),
                    "done".to_string(),
                ),
                parm_list: Some(vec![
                    ParmElem::TernaryExpression(Box::new(
                        Factor::Constant(Constant::True).builder().into(),
                    )),
                    ParmElem::TernaryExpression(Box::new(
                        Factor::Constant(Constant::Integer(42)).builder().into(),
                    )),
                ]),
            }
            .into()],
        };

        let mut pretty_printer_visitor: PrettyPrinterVisitor = Default::default();

        ast.take_visitor_enter_leave(&mut pretty_printer_visitor);

        let expected = indoc! {r#"
            bar.corge(quux, tableNum(CustTable)).baz(grault, new Drix(null, true)).done(true, 42);
        "#};

        let actual = pretty_printer_visitor.into_source();

        assert_eq!(expected, actual);
    }

    #[test]
    fn pretty_print_statements_simple_function_strfmt() {
        let ast = LocalBody {
            dcl_list: vec![],
            stmt_list: vec![Function::StrFmt(vec![
                ParmElem::TernaryExpression(Box::new(
                    Factor::Constant(Constant::String("(%1 == %2)".into()))
                        .builder()
                        .into(),
                )),
                ParmElem::TernaryExpression(Box::new(
                    Factor::Field(Field::Id("AccountNumFoo".into()))
                        .builder()
                        .into(),
                )),
                ParmElem::TernaryExpression(Box::new(
                    Factor::Field(Field::Id("AccountNumBar".into()))
                        .builder()
                        .into(),
                )),
            ])
            .into()],
        };

        let mut pretty_printer_visitor: PrettyPrinterVisitor = Default::default();

        ast.take_visitor_enter_leave(&mut pretty_printer_visitor);

        let expected = indoc! {r#"
            strFmt('(%1 == %2)', AccountNumFoo, AccountNumBar);
        "#};

        let actual = pretty_printer_visitor.into_source();

        assert_eq!(expected, actual);
    }

    #[test]
    fn pretty_print_statements_simple_function_strfmt_with_enum_as_query_value() {
        let ast = LocalBody {
            dcl_list: vec![],
            stmt_list: vec![Function::StrFmt(vec![
                ParmElem::TernaryExpression(Box::new(
                    Factor::Constant(Constant::String("(%1 == %2)".into()))
                        .builder()
                        .into(),
                )),
                ParmElem::TernaryExpression(Box::new(
                    Factor::Intrinsics(Intrinsics::FieldStr(
                        "LogisticsLocation".into(),
                        "Type".into(),
                    ))
                    .builder()
                    .into(),
                )),
                ParmElem::TernaryExpression(Box::new(
                    Factor::Function(Function::QueryValue(ParmElem::TernaryExpression(Box::new(
                        Factor::Constant(Constant::Enum("AddressType".into(), "Postal".into()))
                            .builder()
                            .into(),
                    ))))
                    .builder()
                    .into(),
                )),
            ])
            .into()],
        };

        let mut pretty_printer_visitor: PrettyPrinterVisitor = Default::default();

        ast.take_visitor_enter_leave(&mut pretty_printer_visitor);

        let expected = indoc! {r#"
            strFmt('(%1 == %2)', fieldStr(LogisticsLocation, Type), queryValue(AddressType::Postal));
        "#};

        let actual = pretty_printer_visitor.into_source();

        assert_eq!(expected, actual);
    }
}
