use crate::pretty_printer_visitor::*;
use dracaena::visitor::visitable::Visitable;
use dracaena::ast::top_level_ast_node::*;

pub struct PrettyPrinter {
    pretty_printer_visitor: PrettyPrinterVisitor,
}

impl PrettyPrinter {

    pub fn new(options: PrettyPrintOptions) -> Self {
        Self {
            pretty_printer_visitor: PrettyPrinterVisitor::new(options)
        }
    }

    pub fn pretty_print(&mut self, ast_top_level: &TopLevelAstNode) -> String {
        match ast_top_level {
            TopLevelAstNode::SelectStmt(select_stmt) => {
                select_stmt.take_visitor_enter_leave(&mut self.pretty_printer_visitor);
                let ppv = std::mem::take(&mut self.pretty_printer_visitor);
                ppv.into_source()
            },
            TopLevelAstNode::LocalBody(local_body) => {
                local_body.take_visitor_enter_leave(&mut self.pretty_printer_visitor);
                let ppv = std::mem::take(&mut self.pretty_printer_visitor);
                ppv.into_source()
            }
        }
    }
}